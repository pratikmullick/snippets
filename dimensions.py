"""
This simple script creates a blank curses window and displays the window size
at the bottom right corner of the screen. Press q to exit.
"""


import curses



# Init screen
screen = curses.initscr()

# Disable all cursor and characters being shown on screen
curses.curs_set(0)
curses.cbreak()
curses.noecho()

# Get initial screen values, print and refresh screen
num_rows, num_cols = screen.getmaxyx()
screen.clear()
screen.addstr(num_rows - 1, num_cols - 3, str(num_rows))
screen.addstr(num_rows - 2, num_cols - 3, str(num_cols))
screen.refresh()


# Run loop for subsequent value changes
while True:
    new_rows, new_cols = screen.getmaxyx()
    if new_rows != num_rows or new_cols != num_cols:
        screen.clear()
        screen.addstr(new_rows - 1, new_cols - 3, str(new_rows))
        screen.addstr(new_rows - 2, new_cols - 3, str(new_cols))
        screen.refresh()
        num_rows = new_rows
        num_cols = new_cols
    elif screen.getch() == ord("q"):
        curses.curs_set(1)
        curses.nocbreak()
        curses.echo()
        curses.endwin()
        break

