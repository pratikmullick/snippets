#!/usr/bin/env python3

"""
This simple script converts spaces and hyphen characters in multiple files to
underscore characters. The script requires a directory as its input to the 
command line. Tested with Python3.9 on Arch Linux x86_64.
"""

import os,sys

try:
    directory = os.path.abspath(sys.argv[1])
    new_filename = ''
    for old_filename in os.listdir(directory):
        if ' ' in old_filename:
            new_filename = old_filename.replace(' ','_')
            os.rename(os.path.join(directory,old_filename),os.path.join(directory,new_filename))
        elif '-' in old_filename:
            new_filename = old_filename.replace('-','_')
            os.rename(os.path.join(directory,old_filename),os.path.join(directory,new_filename))
except IndexError:
    print("Missing Directory.")
