# Snippets
Copyright 2021. Pratik Mullick.

## Introduction
These are some simple scripts written for localized use cases. Most scripts
contain an introduction, and their use cases; as comments in the source code.
USE THEM AT YOUR OWN RISK.

## License
Refer to COPYING for more information.
